pipeline {
  agent any;

  options {
    buildDiscarder(logRotator(numToKeepStr: '6'))
  }

  stages {
    stage('Setup Environment') {
      steps {
        script {
          env.EXIT_STATUS = ''
          env.CURR_DATE = sh(
            script: '''date '+%Y-%m-%d_%H:%M:%S%:z' ''',
            returnStdout: true).trim()
          env.GITHASH_SHORT = sh(
            script: '''git log -1 --format=%h''',
            returnStdout: true).trim()
          env.GITHASH_LONG = sh(
            script: '''git log -1 --format=%H''',
            returnStdout: true).trim()
          env.GIT_TAG_NAME = sh(
            script: '''git describe --tags ${commit}''',
            returnStdout: true).trim()
          currentBuild.displayName = "${GIT_TAG_NAME}"
        }
      }
    }
    stage('Update Helm Chart') {
      steps {
        script {
          sh '''
            # Update the Chart Version with the tag from git
            sed -i 's/version:.*/version: '"$GIT_TAG_NAME"'/g' helm/Chart.yaml
            
            helm lint helm            

            helm package helm
            helm push-artifactory *.tgz tkf-charts
          '''
        }
      }
    }
  }
  post {
    always {
      echo 'Cleaning up.'
      deleteDir() /* clean up our workspace */
    }
  }
}
